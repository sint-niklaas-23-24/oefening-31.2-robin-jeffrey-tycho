﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_31._2
{
    internal class Auto
    {
		private int _litersInTank;
		private int _snelheid;

		public delegate void AutoHandler(string Sender);

        public event AutoHandler AutoGestartEvent; 
		public event AutoHandler AutoGestoptEvent; 
		public event AutoHandler TankBijnaOpEvent; 
		public event AutoHandler TankChangeEvent;
        public event AutoHandler TankOpEvent;

		public Auto() 
		{ 
			_litersInTank = 40;
		}

		public int LitersInTank
		{
			get { return _litersInTank; }
			set {
                _litersInTank = value;

				if (TankChangeEvent != null)
				{
                    TankChangeEvent(value.ToString());
                }

                if (value <= 0)
                {
                    if (TankOpEvent != null)
					{
						TankOpEvent("Tank is op! Auto wordt stilgelegd om ernstige schade te voorkomen.");
					}
                }
				else if (value > 0 && value <= 10)
				{
					if (TankBijnaOpEvent != null)
					{
						TankBijnaOpEvent("Piep piep! Tank bijna op." + Environment.NewLine + "Gelieve u naar het dichtstbijzijnde tankstation te begeven.");
					}
				}
			}
		}

		public int Snelheid
		{
			get { return _snelheid; }
			set { 
				_snelheid = value; 
				if (value > 0)
				{
					if (AutoGestartEvent != null)
					{
						AutoGestartEvent(value.ToString());
					}
				}
				else
				{
					if (AutoGestoptEvent != null)
					{
						AutoGestoptEvent(value.ToString());
					}
				}
			}
		}

        public override string ToString()
        {
			return "";
        }

		public void VerminderAantalLiters()
		{
			LitersInTank--;
		}

    }
}
