﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Oefening_31._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        DispatcherTimer timer;
        Auto auto;

        public MainWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += Timer_Tick; ;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            auto.LitersInTank -= 5;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            auto.Snelheid = 75;
            timer.Start();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            auto.Snelheid = 0;
            timer.Stop();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            auto = new Auto();
            lblSnelheid.Content = auto.Snelheid;
            lblLitersInTank.Content = auto.LitersInTank;
            auto.AutoGestartEvent += Auto_AutoGestartEvent;
            auto.AutoGestoptEvent += Auto_AutoGestoptEvent;
            auto.TankChangeEvent += Auto_TankChangeEvent;
            auto.TankBijnaOpEvent += Auto_TankBijnaOpEvent;
            auto.TankOpEvent += Auto_TankOpEvent;
        }

        private void Auto_TankOpEvent(string Sender)
        {
            timer.Stop();
            grdWindow.Background = Brushes.Red;
            MessageBox.Show(Sender);
            auto.Snelheid = 0;
            auto.LitersInTank = 5;
        }

        private void Auto_TankBijnaOpEvent(string Sender)
        {
            lblWarning.Content = Sender;
            grdWindow.Background = Brushes.Orange;
        }

        private void Auto_TankChangeEvent(string Sender)
        {
            lblLitersInTank.Content = Sender;
        }

        private void Auto_AutoGestoptEvent(string Sender)
        {
            lblSnelheid.Content = Sender;
        }

        private void Auto_AutoGestartEvent(string Sender)
        {
            lblSnelheid.Content = Sender;
        }
    }
}
